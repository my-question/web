# My Question web app

## Tecnologias utilizadas
React com JavaScript, Styled Components, Styled System e axios.

## Figma
As telas do projeto podem ser contradas [**aqui**](https://www.figma.com/file/msFA8uibG3tTWg2uGrkjlR/Untitled?node-id=0%3A1).

## Siga os passos abaixo para executar o projeto

1) Renomeie o arquivo da raiz do projeto chamado .env.example para .env. Por padrão a API deste projeto roda na URL que já esta defina neste arquivo. 

2) Inicie as dependências do projeto
```
 yarn install
```

3) Inicie o servidor
```
yarn start
``` 
## Documentação
Mais informações do projeto [**aqui**](https://gitlab.com/my-question/doc).

## Estrutura de diretórios

```
├── /src
|   ├── /components
|   ├── /context
|   ├── /helpers
|   ├── /providers
|   ├── /screens
|   |   ├── /Questionnaire
|   |   |   ├── /Home
|   |   |   ├── /Questionnaire
|   |   |   ├── /QuestionnaireForm
|   |   |   ├── /QuestionnaireList
|   |   |   ├── /QuestionnairesManagement
|   |   ├── /User
|   |   |   ├── /Login 
|   |   |   ├── /PasswordReset
|   |   |   ├── /SendEmail
|   |   |   ├── /UserForm
|   |   |   ├── /UserProfile
|   ├── /services
|   ├── /theme
```