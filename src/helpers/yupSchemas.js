import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

const yupShapeWithResolver = shape => yupResolver(yup.object().shape(shape))

export const loginSchema = yupShapeWithResolver({
  email: yup
    .string()
    .email('Insira um e-mail válido')
    .required('Insira seu e-mail'),
  password: yup
    .string()
    .min(6, 'Mínimo de 6 caracteres no campo')
    .required()
})

export const signupSchema = yupShapeWithResolver({
  email: yup
    .string()
    .email('Insira um e-mail válido')
    .required('Insira seu e-mail'),
  password: yup
    .string()
    .min(6, 'Mínimo de 6 caracteres no campo')
    .required()
})

export const sendEmailSchema = yupShapeWithResolver({
  email: yup
    .string()
    .email('Insira um e-mail válido')
    .required('Insira seu e-mail')
})

export const passwordResetSchema = yupShapeWithResolver({
  password: yup
    .string()
    .min(6, 'Mínimo de 6 caracteres no campo')
    .required(),
  token: yup
    .string()
    .min(8, 'Informe o código de 8 caracteres')
    .max(8, 'Informe o código de 8 caracteres')
    .required()
})
