import { Routes, Route, Navigate } from 'react-router-dom'

import {
  Home,
  Questionnaire,
  QuestionnaireForm,
  QuestionnaireList,
  QuestionnairesManagement
} from 'screens'

const AuthenticatedApp = () => {
  return (
    <Routes>
      <Route path='/home' element={<Home />} />
      <Route exact path='/questionnaire' element={<Questionnaire />} />
      <Route exact path='/questionnaire/create' element={<QuestionnaireForm />} />
      <Route exact path='/questionnaire/list' element={<QuestionnaireList />} />
      <Route exact path='/questionnaire/management' element={<QuestionnairesManagement />} />

      <Route path='*' element={<Navigate to='/home' />} />
    </Routes>
  )
}

export default AuthenticatedApp
