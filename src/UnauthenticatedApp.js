import { Routes, Route, Navigate } from 'react-router-dom'

import { Login, UserForm, SendEmail, PasswordReset } from 'screens'

const UnauthenticatedApp = () => {
  return (
    <Routes>
      <Route path='/login' element={<Login />} />
      <Route path='/signup' element={<UserForm />} />
      <Route path='/users/regain-access/send-email' element={<SendEmail />} />
      <Route path='/users/regain-access/password-reset' element={<PasswordReset />} />

      <Route path='*' element={<Navigate to='/login' />} />
    </Routes>
  )
}

export default UnauthenticatedApp
