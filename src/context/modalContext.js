import React, { useCallback, useMemo, useState } from 'react'

import { Modal, Row, Text, Icon, Button, Column } from 'components'

const CONFIRMATION_TYPE = 'confirmation'

const ModalContext = React.createContext()

const useModal = () => {
  const context = React.useContext(ModalContext)
  if (context === undefined)
    throw new Error('useModal deve ser usado dentro de um ModalProvider')

  return context
}

const ModalProvider = ({ children }) => {
  const [modalData, setModalData] = useState(null)

  const handleOpenModal = newModalData => setModalData(newModalData)

  const handleCloseModal = () => {
    if (modalData?.onClose) {
      modalData.onClose()
    }
    setModalData(null)
  }

  const handleConfirm = useCallback(() => {
    modalData?.onConfirm()
  }, [modalData])

  const isConfirmationType = useMemo(
    () => modalData?.type === CONFIRMATION_TYPE,
    [modalData]
  )

  const modalContent = useMemo(
    () => ({
      title: modalData?.title,
      content: modalData?.content
    }),
    [modalData]
  )

  return (
    <ModalContext.Provider value={{ handleOpenModal, handleCloseModal }}>
      {children}
      <Modal isOpen={!!modalData} height={isConfirmationType ? 200 : 160}>
        <Column ml={32}>
          <Row>
            <Text variant='regular' fontWeight={600} mt={32}>
              {modalContent.title}
            </Text>
          </Row>
          <Row>
            <Text fontSize={16} lineHeight='36px' mt={22}>
              {modalContent.content}
            </Text>
          </Row>
          {isConfirmationType ? (
            <Row position='absolute' mr={22} mb={22} bottom={0} right={0}>
              <Button
                onClick={() => handleCloseModal()}
                variant='primary'
                color='#212121'
                bg='#FFFFFF'
              >
                Cancelar
              </Button>
              <Button onClick={handleConfirm} ml={24} variant='primary'>
                Excluir
              </Button>
            </Row>
          ) : (
            <Row position='absolute' mt={22} mr={22} right={0}>
              <Icon name='close' onClick={() => handleCloseModal()} />
            </Row>
          )}
        </Column>
      </Modal>
    </ModalContext.Provider>
  )
}

export { ModalProvider, useModal }
