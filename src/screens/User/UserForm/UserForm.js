import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { useNavigate } from 'react-router-dom'

import { Column, Logo, Text, Input, Button, Loader, Row, Link, Footer } from 'components'
import { signupSchema } from 'helpers/yupSchemas'
import { createUser } from 'services'

const UserForm = () => {
  const navigate = useNavigate()

  const { mutate: create, isLoading } = useMutation(createUser, {
    onSuccess: () => navigate('/login')
  })

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ resolver: signupSchema })

  return (
    <Column alignItems='center'>
      <Column
        as='form'
        onSubmit={handleSubmit(create)}
        alignItems='center'
        overflow='auto'
      >
        <Logo width={186} height={71} mt={32} />
        <Text fontSize={18} fontWeight={600} mt={32}>Inscreva-se</Text>
        <Input
          name='email'
          label='E-mail'
          placeholder='exemplo@domínio.com'
          type='email'
          ref='ref'
          {...register('email')}
          error={errors.email?.message}
        />
        <Input
          name='password'
          label='Senha'
          placeholder='*********'
          type='password'
          ref='ref'
          {...register('password')}
          error={errors.password?.message}
        />
        {isLoading ? <Loader /> : <Button type='submit'>Cadastre-se</Button>}
      </Column>
      <Row mt={10} mb={183}>
        <Text>Já tem uma conta? </Text>
        <Link label='Entre' to={'/login'} fontWeight={600} ml={1} />
      </Row>
      <Footer />
    </Column>
  )
}

export default UserForm
