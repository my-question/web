import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { useNavigate } from 'react-router-dom'

import { Column, Logo, Text, Input, Button, Row, Link, Loader, Footer } from 'components'
import { sendEmailSchema } from 'helpers/yupSchemas'
import { sendToken as sendTokenService } from 'services'

const SendEmail = () => {
  const navigate = useNavigate()

  const {
    mutate: sendToken,
    isLoading,
    isError
  } = useMutation(sendTokenService, {
    onSuccess: () => navigate('/users/regain-access/password-reset')
  })

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ resolver: sendEmailSchema })

  return (
    <Column alignItems='center' overflow='auto'>
      <Logo width={186} height={71} mt={32} />
      {isError && (
        <Row position='absolute' color='red' mt={110}>
          <Text>E-mail informado não cadastrado</Text>
        </Row>
      )}
      <Text fontSize={18} fontWeight={600} mt={32}>Recuperar acesso</Text>
      <Column as='form' onSubmit={handleSubmit(sendToken)} alignItems='center'>
        <Text width={296} my={22}>
          Informe o e-mail cadastrado para receber o código de verificação
        </Text>
        <Input
          name='email'
          label='E-mail'
          placeholder='exemplo@domínio.com'
          type='email'
          ref='ref'
          {...register('email')}
          error={errors.email?.message}
        />
        {isLoading ? <Loader /> : <Button type='submit'>Receber código de validação</Button>}
      </Column>
      <Row mt={10} mb={183}>
        <Link label='Entre' to={'/login'} fontWeight={600} mr={2} />
        <Text>-</Text>
        <Link label='Cadastre-se' to={'/signup'} fontWeight={600} ml={2} />
      </Row>
      <Footer />
    </Column>
  )
}

export default SendEmail
