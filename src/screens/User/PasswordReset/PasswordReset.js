import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { useNavigate } from 'react-router-dom'

import { Column, Logo, Text, Input, Button, Row, Link, Loader, Footer } from 'components'

import { passwordReset as passwordResetService } from 'services'
import { passwordResetSchema } from 'helpers/yupSchemas'
import { useModal } from 'context/modalContext'

const PasswordReset = () => {
  const navigate = useNavigate()
  const { handleOpenModal } = useModal()

  const {
    mutate: passwordReset,
    isLoading,
    isError
  } = useMutation(passwordResetService, {
    onSuccess: () => {
      handleOpenModal({
        type: 'success',
        title: 'Recuperar acesso',
        content: 'Senha alterada com sucesso!',
        onClose: () => navigate('/users/login')
      })
    }
  })

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ resolver: passwordResetSchema })

  return (
    <Column alignItems='center' overflow='auto'>
      <Logo width={186} height={71} mt={32} />
      {isError && (
        <Row position='absolute' color='red' mt={110}>
          <Text>Código informado incorreto</Text>
        </Row>
      )}
      <Text fontSize={18} fontWeight={600} mt={32}>Recuperar acesso</Text>
      <Column as='form' onSubmit={handleSubmit(passwordReset)} alignItems='center'>
        <Text width={296} my={22}>
          Informe o código de verificação recebido no e-mail
        </Text>
        <Input
          name='token'
          label='Código de verificação'
          placeholder='Código de verificação'
          type='token'
          ref='ref'
          {...register('token')}
          error={errors.token?.message}
        />
        <Input
          name='password'
          label='Nova senha'
          placeholder='*********'
          type='password'
          ref='ref'
          {...register('password')}
          error={errors.password?.message}
        />
        {isLoading ? <Loader /> : <Button type='submit'>Cadastrar nova senha</Button>}
        <Row mt={10} mb={183}>
          <Link label='Entre' to={'/login'} fontWeight={600} mr={2} />
          <Text>-</Text>
          <Link label='Cadastre-se' to={'/signup'} fontWeight={600} ml={2} />
        </Row>
      </Column>
      <Footer />
    </Column>
  )
}

export default PasswordReset
