import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'

import { Column, Logo, Text, Input, Button, Row, Link, Icon, Loader, Footer } from 'components'
import { loginSchema } from 'helpers/yupSchemas'
import { useUser } from 'context/userContext'

const UserForm = () => {
  const { loginContext } = useUser()

  const { mutate: login, isLoading, isError } = useMutation(loginContext)

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ resolver: loginSchema })

  return (
    <Column alignItems='center'>
      <Column
        as='form'
        onSubmit={handleSubmit(login)}
        alignItems='center'
        overflow='auto'
      >
        <Logo width={186} height={71} mt={32} />
        {isError && (
          <Row position='absolute' color='red' mt={110}>
            <Text>E-mail e/ou senha inválido(s)</Text>
          </Row>
        )}
        <Text fontSize={18} fontWeight={600} mt={32}>Entrar</Text>
        <Input
          name='email'
          label='E-mail'
          placeholder='exemplo@domínio.com'
          type='email'
          ref='ref'
          {...register('email')}
          error={errors.email?.message}
        />
        <Input
          name='password'
          label='Senha'
          placeholder='*********'
          type='password'
          ref='ref'
          {...register('password')}
          error={errors.password?.message}
        />
        {isLoading ? <Loader /> : <Button type='submit'>Entrar</Button>}
      </Column>
      <Link to={'/users/regain-access/send-email'} mt={22}>
        <Text variant='small' fontWeight={600}>Esqueceu a senha?</Text>
      </Link>
      <Row mt={10}>
        <Text variant='small' fontWeight={600} mt={12} mr={3}>Ou acesse com:</Text>
        <Link mr={3}>
          <Icon name='google' />
        </Link>
        <Link>
          <Icon name='facebook' />
        </Link>
      </Row>
      <Row mt={10} mb={183}>
        <Text>Não tem uma conta?</Text>
        <Link label='Cadastre-se' to={'/signup'} fontWeight={600} ml={1} />
      </Row>
      <Footer />
    </Column>
  )
}

export default UserForm
