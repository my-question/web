import { client } from 'providers'

export const getUser = () => client.get('/me')

export const createUser = async data => 
  await client.post('/users/signup', data)

export const login = async data => 
  await client.post('/users/login', data)

export const sendToken = async data => 
  await client.post('/users/forget', data)

export const passwordReset = async data =>
  await client.post('/users/reset', data)
