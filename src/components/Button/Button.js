import styled from 'styled-components'
import { space, color, layout, position, variant } from 'styled-system'
import propTypes from '@styled-system/prop-types'

import { MEDIATABLET } from 'helpers'

const ButtonComponent = ({ children, ...props }) => 
  <Button {...props}>{children}</Button>

const Button = styled.button`
  width: 296px;
  height: 40px;
  background-color: #212121;
  color: #ffffff;
  font-weight: 600;
  font-size: 14px;
  border-radius: 5px;
  border: 1px solid ${({ theme }) => theme.buttonBorder};
  ${space}
  ${color}
  ${layout}
  ${position}
  ${variant({
    variants: {
      primary: {
        width: '115px'
      }
    }
  })}
  
  @media (min-width: ${MEDIATABLET}px) {
    ${variant({
    variants: {
      primary: {
        width: '176px'
      }
    }
  })}
  }
`

ButtonComponent.propTypes = {
  ...propTypes.space,
  ...propTypes.layout,
  ...propTypes.position,
  ...propTypes.variant
}

export default ButtonComponent
