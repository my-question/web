import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { variant } from 'styled-system'
import propTypes from '@styled-system/prop-types'

import { Column, Text } from 'components'

const InputComponent = 
  forwardRef(({ label, name, placeholder, error, type, ...props }, ref) => (
    <Column mb={32}>
      <Column position='relative'>
        {label && (
          <Text variant='small' fontWeight={600} mb={2}>{label}</Text>
        )}
        <Input 
          name={name} 
          placeholder={placeholder} 
          type={type} 
          error={error} 
          ref={ref} 
          {...props}
        />
        <Text 
          variant='small' 
          position='absolute' 
          top={68} 
          color='#FF0000'
        >
          {error}
        </Text>
      </Column>
    </Column>
  ))

const Input = styled.input`
  width: 296px;
  height: 40px;
  padding: 8px;
  font-size: 16px;
  border-radius: 5px;
  background-color: #F5F5F5;
  box-shadow: 0px 4px 4px ${({ theme }) => theme.shadow};
  ${variant({ 
    variants: { 
      primary: { width: '384px' } 
    }
  })}
`

InputComponent.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  register: PropTypes.func,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  type: PropTypes.string,
  ...propTypes.variant
}

export default InputComponent
