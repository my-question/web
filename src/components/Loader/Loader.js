import styled, { keyframes } from 'styled-components'

const LoaderComponent = () => (
  <OuterCircle>
    <MiddleCircle>
      <InnerCircle />
    </MiddleCircle>
  </OuterCircle>
)

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const reverse360 = keyframes`
  from {
    transform: rotate(1turn);
  }
  to {
    transform: rotate(-2turn);
  }
`

const OuterCircle = styled.div`
  animation: ${rotate360} 1.5s linear infinite;
  transform: translateZ(0);
  border-left: 2px solid ${({ theme }) => theme.loader};
  border-top: 2px solid ${({ theme }) => theme.loader};
  border-right: 2px solid transparent;
  border-bottom: 2px solid transparent;
  background: transparent;
  width: 39.99px;
  height: 39.99px;
  border-radius: 50%;
`

const MiddleCircle = styled.div`
  animation: ${reverse360} 2s linear infinite;
  transform: scale(2);
  border-left: 2px solid ${({ theme }) => theme.loader};
  border-top: 2px solid ${({ theme }) => theme.loader};
  border-right: 2px solid transparent;
  border-bottom: 2px solid transparent;
  background: transparent;
  width: 26.66px;
  height: 26.66px;
  border-radius: 50%;
  margin: 5px;
`

const InnerCircle = styled.div`
  animation: ${rotate360} 5s linear infinite;
  transform: translateZ(0);
  border-left: 2px solid ${({ theme }) => theme.loader};
  border-top: 2px solid ${({ theme }) => theme.loader};
  border-right: 2px solid transparent;
  border-bottom: 2px solid transparent;
  background: transparent;
  width: 13.33px;
  height: 13.33px;
  border-radius: 50%;
  margin: 5px;
`

export default LoaderComponent
