import styled from 'styled-components'
import { space, layout, typography, color, position, variant } from 'styled-system'
import propTypes from '@styled-system/prop-types'

const SMALL = 'small'
const BIG = 'big'
const REGULAR = 'regular'

const TextComponent = styled.p(
  space,
  layout,
  typography,
  color,
  position,
  variant({
    variants: {
      [SMALL]: {
        fontSize: '14px',
        lineHeight: '17px'
      },
      [REGULAR]: {
        fontSize: '16px',
        lineHeight: '24px'
      },
      [BIG]: {
        fontSize: '24px',
        lineHeight: '36px'
      }
    }
  })
)

TextComponent.propTypes = {
  ...propTypes.space,
  ...propTypes.layout,
  ...propTypes.typography,
  ...propTypes.color,
  ...propTypes.position,
  ...propTypes.variant
}

export default TextComponent
