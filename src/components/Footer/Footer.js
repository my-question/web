import styled from 'styled-components'

import { Column, Logo, Text } from 'components'
import { MEDIATABLET } from 'helpers'

const FooterComponent = () => {
  return (
    <Footer>
      <Hr />
      <Column ml={32}>
        <Logo mt={22} />
        <Text mt={22}>©2022 My Question.</Text>
        <Text>Todos os direitos reservados.</Text>
      </Column>
    </Footer>
  )
}

const Footer = styled.div`
  height: 161px;
  width: 100%;
  max-width: 1280px;
  position: fixed;
  bottom: 0px;
  background-color: ${({ theme }) => theme.background};
`

const Hr = styled.hr`
  border: 0;
  width: 90%;
  margin: auto;
  border-top: 1px solid ${({ theme }) => theme.divider};
  @media (min-width: ${MEDIATABLET}px) {
    width: 95%;
  }
`

export default FooterComponent
