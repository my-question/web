import { useMemo } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import Sun from './Sun'
import Moon from './Moon'
import Close from './Close'
import Google from './Google'
import Facebook from './Facebook'

const IconComponent = ({ name, onClick, ...props }) => {
  const Icon = useMemo(() => {
    switch (name) {
    case 'moon':
      return Moon
    case 'sun':
      return Sun
    case 'close':
      return Close
    case 'google':
      return Google
    case 'facebook':
      return Facebook
    }
  }, [name])

  if (typeof onClick !== 'function') {
    return <Icon {...props} />
  }

  return (
    <Button onClick={onClick}>
      <Icon {...props} />
    </Button>
  )
}

const Button = styled.button`
  background: transparent;
`

IconComponent.propTypes = {
  name: PropTypes.string,
  onClick: PropTypes.func,
}

export default IconComponent
