import styled from 'styled-components'

import Svg from './Svg'

const Facebook = ({ ...props }) => {
  return (
    <Circle>
      <Svg width={32} height={32} fill='none' viewBox='0 0 32 32' {...props}>
        <path
          d='M16 0.166656C11.8007 0.166656 7.77347 1.83481 4.80414 4.80413C1.83481 7.77346 0.166664 11.8007 0.166664 16C0.166664 20.1993 1.83481 24.2265 4.80414 27.1958C7.77347 30.1652 11.8007 31.8333 16 31.8333C20.1993 31.8333 24.2265 30.1652 27.1959 27.1958C30.1652 24.2265 31.8333 20.1993 31.8333 16C31.8333 11.8007 30.1652 7.77346 27.1959 4.80413C24.2265 1.83481 20.1993 0.166656 16 0.166656V0.166656Z'
          fill='#039BE5'
        />
        <path
          d='M18.1433 20.1967H22.2408L22.8842 16.0342H18.1425V13.7592C18.1425 12.03 18.7075 10.4967 20.325 10.4967H22.9242V6.86418C22.4675 6.80251 21.5017 6.66751 19.6767 6.66751C15.8658 6.66751 13.6317 8.68001 13.6317 13.265V16.0342H9.71416V20.1967H13.6317V31.6375C14.4075 31.7542 15.1933 31.8333 16 31.8333C16.7292 31.8333 17.4408 31.7667 18.1433 31.6717V20.1967Z'
          fill='#FFFFFF'
        />
      </Svg>
    </Circle>
  )
}

const Circle = styled.div`
  border: 1px solid ${({ theme }) => theme.iconBorder};
  width: 40px;
  height: 40px;
  border-radius: 50%;
  padding: 3px;
`

export default Facebook
