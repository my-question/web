import styled from 'styled-components'
import { space, layout, position } from 'styled-system'
import propTypes from '@styled-system/prop-types'

const Svg = styled('svg')(space, layout, position)

Svg.propTypes = {
  ...propTypes.space,
  ...propTypes.layout,
  ...propTypes.position
}

export default Svg
