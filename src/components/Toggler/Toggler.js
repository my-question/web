import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Icon as defaultIcon } from 'components'
import { MEDIADESKTOP } from 'helpers'

const ToggleComponent = ({ theme, toggleTheme }) => (
  <Icon onClick={toggleTheme} name={theme === 'light' ? 'moon' : 'sun'} />
)

const Icon = styled(defaultIcon)`
  z-index: 1;
  position: fixed;
  right: 0px;
  bottom: 0px;
  margin: 0px 32px 32px 0px;

  @media (min-width: ${MEDIADESKTOP}px) {
    position: absolute;
    margin-left: auto;
    margin-right: auto;
    left: 1180px;
  }
`

ToggleComponent.propTypes = {
  theme: PropTypes.string,
  toggleTheme: PropTypes.func
}

export default ToggleComponent
