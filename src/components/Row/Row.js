import styled from 'styled-components'
import { space, layout, flexbox, position, border, grid, color } from 'styled-system'
import propTypes from '@styled-system/prop-types'

const RowComponent = styled.div(
  {
    display: 'flex'
  },
  flexbox,
  space,
  layout,
  position,
  border,
  grid,
  color
)

RowComponent.propTypes = {
  ...propTypes.flexbox,
  ...propTypes.space,
  ...propTypes.layout,
  ...propTypes.position,
  ...propTypes.border,
  ...propTypes.grid,
  ...propTypes.color
}

export default RowComponent
