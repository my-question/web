import { createPortal } from 'react-dom'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

import { Column } from 'components'
import { MEDIATABLET } from 'helpers'

const ModalComponent = ({ children, isOpen, ...props }) => {
  const portalRef = document.body

  return createPortal(
    <ModalBackground isOpen={isOpen}>
      <Modal {...props}>{children}</Modal>
    </ModalBackground>,
    portalRef
  )
}

const ModalBackground = styled('div')(
  {
    zIndex: 2,
    top: '0px',
    left: '0px',
    position: 'fixed',
    width: '100%',
    height: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    padding: '15px'
  },
  ({ isOpen }) => css`
    visibility: ${isOpen ? 'visible' : 'hidden'};
    opacity: ${isOpen ? 1 : 0};
  `
)

const Modal = styled(Column)`
  width: 300px;
  height: 160px;
  border-radius: 5px;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  margin: auto;
  color: #212121;
  background-color: #ffffff;
  position: absolute;

  @media (min-width: ${MEDIATABLET}px) {
    width: 592px;
  }
`

ModalComponent.propTypes = {
  isOpen: PropTypes.bool
}

export default ModalComponent
