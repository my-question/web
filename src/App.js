import { BrowserRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'

import { useUser } from 'context/userContext'
import { useDarkMode } from 'hooks'
import { GlobalStyles, darkTheme, lightTheme } from './theme'
import { Toggler } from 'components'
import UnauthenticatedApp from 'UnauthenticatedApp'
import AuthenticatedApp from 'AuthenticatedApp'

const App = () => {
  const { user } = useUser()
  const [theme, themeToggler] = useDarkMode()

  const themeMode = theme === 'light' ? lightTheme : darkTheme

  return (
    <ThemeProvider theme={themeMode}>
      <GlobalStyles />
      <Toggler theme={theme} toggleTheme={themeToggler} />
      <Router>{user ? <AuthenticatedApp /> : <UnauthenticatedApp />}</Router>
    </ThemeProvider>
  )
}

export default App
