export const lightTheme = {
  background: '#FFFFFF',
  text: '#212121',
  shadow: 'rgba(0, 0, 0, 0.25)',
  buttonBorder: '#212121',
  iconBorder: '#212121',
  loader: '#212121',
  divider: '#212121'
}

export const darkTheme = {
  //background: '#1C2025',
  //background: '#363537',
  background: '#424242',
  text: '#FFFFFF',
  shadow: 'rgba(255, 255, 255, 0.25)',
  buttonBorder: '#FFFFFF',
  iconBorder: '#FFFFFF',
  loader: '#FFFFFF',
  divider: '#FFFFFF'
}
