import { createGlobalStyle } from 'styled-components'

const globalStyles = createGlobalStyle`
  * {
    border: 0;
    padding: 0;
    margin: 0;
    outline: none;
    box-sizing: border-box;
    font-family: 'Montserrat', sans-serif;
  }
  body {
    background-color: ${({ theme }) => theme.background};
    color: ${({ theme }) => theme.text};
    transition: all 0.05s linear;
  }
  button, a, li {
    cursor: pointer;
  }
`
export default globalStyles
